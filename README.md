# stm32_example
Provides an example of an STM32 project. It uses:

- Eclipse
- GNU ARM toolchain
- QEMU hardware emulator

# Installation 
This installation guide is tested with Ubuntu 18.04. For Windows and macOs there are tutorials available too, 
check the provided links.

## Eclipse
https://websiteforstudents.com/how-to-install-eclipse-oxygen-ide-on-ubuntu-167-04-17-10-18-04/
- Install Java JDK8: Open terminal and type: `sudo add-apt-repository ppa:webupd8team/java`
- Oracle installer: `sudo apt update` followed by `sudo apt install oracle-java8-installer`. Choose yes when asked
- Set JDK as default: `sudo apt install oracle-java8-set-default`
- Test with: `javac -version`
- Download eclipse from https://www.eclipse.org/downloads/
- Extract downloaded package with: `tar xfz ~/Downloads/eclipse-inst-linux64.tar.gz`
- Run Eclipse installer: `sudo ~/Downloads/eclipse-installer/eclipse-inst` and follow its instructions
- Go to *${HOME}/.local/share/applications* and create a file called *eclipse.desktop*
- Open it with editor and copy paste this text:

```
[Desktop Entry]
Name=Eclipse CPP Oxygen
Type=Application
Exec=/your_eclipse_directory/eclipse
Terminal=false
Icon=/your_eclipse_directory/icon.xpm
Comment=Integrated Development Environment
NoDisplay=false
Categories=Development;IDE;
Name[en]=Eclipse
```

- Replace *your_eclipse_directory* with your installation directory
- Change permissions of *root/.p2* with: `sudo chmod 775 -R /root/`

## npm and xpm
See https://www.npmjs.com/package/xpm: 
- `sudo apt install npm`
- `mkdir -p "${HOME}"/opt/npm`
- `npm config set prefix "${HOME}"/opt/npm`
- `echo 'export PATH="${HOME}"/opt/npm/bin:${PATH}' >> "${HOME}"/.profile`
- `source "${HOME}"/.profile`
- `npm install --global xpm`
- test with `xpm --version`

## GNU ARM toolchain and its plugin
https://gnu-mcu-eclipse.github.io/toolchain/arm/install/:
- `xpm install --global @gnu-mcu-eclipse/arm-none-eabi-gcc`
- In Eclipse, go to *help->Eclipse Marketplace*, type and install **GNU MCU Eclipse**

## Install QEMU
https://gnu-mcu-eclipse.github.io/qemu/install/
- Get and install QEMU: `xpm install --global @gnu-mcu-eclipse/qemu`

## Get CMSIS packages
Install Required packages, see https://gnu-mcu-eclipse.github.io/plugins/packs-manager/:
- Open the packs view in *Windows->View->Other->Packs*
- Update your packages by clicking the yellow arrow refresh button. It is located at the upper right edge of the center window.
- Now install your device packages

# Create new project example
- In Eclipse click *File->New->C++ Project*
- Name it and select **STM32F4xx C/C++ Project**
- Use **Cross ARM GCC** and click *Next*
- Select **STM32F407xx** family and **Semihosting (POSIX...)**, then *Next*
- Click *Next* multiple times until you see the **Cross GNU ARM Toolchain** window
- Select **GNU Tools for ARM...** and set the toolchain path to your *gcc-arm-none-eabi/../bin* directory
- Click *finish* and build the project
- Right click on project and select **Properties**
- Go to *C/C++ Build->Settings->Devices* and select **STM32F407VG** and click *ok*
- Right click project and select *Run as->Run configurations*
- Double click **GDB QEMU Debugging** and select the *Debugger* tab
- Check board name and enable **Extra verbose**
- *Apply* and *run*
- The board should appear as an image and LED is blinking
